import { writeAll } from "https://deno.land/std@0.106.0/io/util.ts";

const LIMIT = 100;
const MS_PER_SECOND = 1_000;
const DEFAULT_RATE_LIMIT_WAIT = 60 * MS_PER_SECOND;
const TOKEN = Deno.env.get("GITLAB_TOKEN") || "";
const ANSI_REPLACE_LINE = '\u001B[2K\r';

interface UserEvent {
  action_name: string;
  target_title: string;
  author_username: string;
  note: {
    id: number;
    type: string;
    body: string;
    created_at: string;
    noteable_id: number;
    noteable_type: string;
    noteable_iid: number;
  };
}

interface PaginatedResponse<T> {
  nextPage: number;
  page: number;
  totalPages: number;
  data: T;
}

interface RetryablePaginatedResponse<T> {
  response?: PaginatedResponse<T>;
  retryAfter?: number;
}

type FetchedUserEvents = PaginatedResponse<UserEvent[]>;

enum HTTPStatus {
  TOO_MANY_REQUESTS = 429,
}

const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

const writeToStdOut = (message: string) =>
  writeAll(Deno.stdout, new TextEncoder().encode(message));

const getUserEventsApiUrl = (username: string, page: number) => {
  return `https://gitlab.com/api/v4/users/${username}/events?action=commented&per_page=${LIMIT}&page=${page}`;
};

const logFetchProgress = (page: number, totalPages: number) => {
  const message = Number.isInteger(totalPages)
    ? `${ANSI_REPLACE_LINE}Fetching page ${page} of ${totalPages}...`
    : `Fetching page ${page}...`;

  writeAll(Deno.stderr, new TextEncoder().encode(message));
}

const parsePagination = (response: Response) => {
  return {
    nextPage: parseInt(response.headers.get("x-next-page") ?? "", 10),
    totalPages: parseInt(response.headers.get("x-total-pages") ?? "", 10),
  };
};

const isUserEvents = (obj: unknown): obj is UserEvent[] =>
  Array.isArray(obj) &&
  obj.length > 0 &&
  Reflect.has(obj[0], "author_username");

const fetchUserEventsPage = async (
  username: string,
  nextPage: number,
): Promise<RetryablePaginatedResponse<UserEvent[]>> => {
  const page = nextPage;

  const apiUrl = getUserEventsApiUrl(username, page);
  const response = await fetch(apiUrl, {
    headers: { "Accept": "application/json", "PRIVATE-TOKEN": TOKEN },
  });

  if (!response.ok) {
    if (response.status === HTTPStatus.TOO_MANY_REQUESTS) {
      const retryAfterResp = Number(response.headers.get("retry-after")) *
        MS_PER_SECOND;
      const retryAfter = Number.isNaN(retryAfterResp)
        ? DEFAULT_RATE_LIMIT_WAIT
        : retryAfterResp;

      return {
        retryAfter,
      };
    } else {
      throw new Error(`Unexpected error status ${response.status}`);
    }
  } else {
    const { nextPage, totalPages } = parsePagination(response);
    const json = await response.json();

    if (isUserEvents(json)) {
      return {
        response: {
          nextPage,
          page,
          totalPages,
          data: json,
        }
      };
    } else {
      throw new Error(`Unexpected JSON payload: ${JSON.stringify(json)}`);
    }
  }
};

async function* fetchUserEvents(
  username: string,
): AsyncGenerator<FetchedUserEvents, void, undefined> {
  let nextPage = 1;
  let totalPages = Infinity;

  while (nextPage <= totalPages) {
    const page = nextPage;

    logFetchProgress(page, totalPages);
    const { retryAfter, response } = await fetchUserEventsPage(username, page);

    if (retryAfter) {
      console.error(
        `Exceeded rate limit; waiting ${
          Math.round(retryAfter / MS_PER_SECOND)
        } seconds...`,
      );
      await sleep(retryAfter);
    } else if (response) {
      ({ nextPage, totalPages } = response);

      yield response;
    } else {
      throw new Error('Expected to either find "response" or "retryAfter".')
    }
  }
}

const main = async ([username]: string[]) => {
  if (!username) {
    console.error("Please provide a username to search for.");
    Deno.exit(1);
    return;
  }

  await writeToStdOut("[");
  for await (const paginatedResponse of fetchUserEvents(username)) {
    if (paginatedResponse.page > 1) {
      await writeToStdOut(",");
    }

    const result = paginatedResponse.data
      .map((x) => JSON.stringify(x))
      .join(",");
    await writeToStdOut(result);
  }
  await writeToStdOut("]");
};

if (import.meta.main) {
  try {
    await main(Deno.args);
  } catch (error) {
    console.error(error);
    Deno.exit(2);
  }
}

// Fix import. bug with tsc https://github.com/denoland/deno/issues/8864
export {};
