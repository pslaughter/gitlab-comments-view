## What do I do here?

### Prerequisites

1. [Install Deno](https://deno.land/#installation)
2. Install [mustache](https://www.npmjs.com/package/mustache), e.g., via `npm install -g mustache`

### How to use

1. Clone this repo
2. `cd` into its directory
3. Run `./run $GITLAB_USERNAME` and replace `$GITLAB_USERNAME` with your GitLab username

Then open `public/$GITLAB_USERNAME/index.html` in your favorite browser and enjoy :)

### Screenshots

![screenshot](./doc/screenshot.png)
